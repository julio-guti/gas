# Profile file. Runs on login.

export PATH="$PATH:$HOME/.local/bin"
export EDITOR="vim"
export TERMINAL="urxvtc"

[ -f ~/.bashrc ] && source ~/.bashrc

# Start graphical server if i3 not already running.
if [ "$(tty)" = "/dev/tty1" ]; then
	pgrep -x i3 || exec startx
fi

wal -Rn
