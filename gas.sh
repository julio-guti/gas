#!/bin/bash

# Guti's Arch Setup
# License: GNU GPLv3

set -x

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

# Install apps

if [ -d ./home ]; then
	# copy over all the home files into $HOME
	rsync -r -v ./home/ $HOME/
fi

# Execute scripts
